var shreepadApp=angular.module('shreepadApp', [ "ngRoute","ngStorage","mobile-angular-ui","ui.bootstrap"]);

var serviceurl = "http://www.shreepadgroup.com/data/";

/* to display special characters*/
shreepadApp.filter('html',function($sce){
    return function(input){
        return $sce.trustAsHtml(input);
    }
})

function openlinks(url)
{
	window.open(url, "_system");
}

function onBackKeyDown()
 {
    navigator.app.exitApp();  
}

shreepadApp.config(function($routeProvider) {
        $routeProvider
            // route for the home page
            .when('/home', {
                templateUrl : 'partials/home.html',
                controller  : 'mainController',
                label: 'Home'
            })
            .when('/success-story', {
                templateUrl : 'partials/success-story.html',
                controller  : 'success-storyController'
            })
            .when('/news', {
                templateUrl : 'partials/news.html',
                controller  : 'newsController'
            })
            .when('/contact-us', {
                templateUrl : 'partials/contact-us.html',
                controller  : 'contactController',
                label: 'Contact Us'
            })
            .when('/about', {
                templateUrl : 'partials/about.html',
                controller  : 'aboutController'                  
            })
            .when('/Project/:projectId', {
                templateUrl : 'partials/project-detail.html',
                controller  : 'projectDetailController',
                label: 'Projects'
            })
            .when('/reach-us', {
                templateUrl : 'partials/reachus.html',
                controller  : 'reachusController',
                label: 'Reach Us'
            })
   	   . when('/ProjectSelect/', {
                templateUrl : 'partials/allprojects.html',
                controller  : 'allprojectController',
                label: 'Projects'
            })
            .otherwise({
            	  templateUrl : 'partials/home.html',
                controller  : 'mainController',
                label: 'Home'
      	   });           
});

/* called on every route*/
shreepadApp.run(function ($rootScope,$localStorage,$http,$location) {
   /* set title for all pages */ 
  $rootScope.title = function(title) 
   {
 		document.getElementById('brand-title').innerHTML =title;
   };
   
   /* set header for all pages */ 	
   $rootScope.header = function(common) 
   {
 		$rootScope.defaultheader = common;
		
   };

    /* set footer for all pages */ 
   $rootScope.footer = function(common) 
   {
 		$rootScope.innerfooter = common;
		
   };
    
	
     /* check for internet connection*/
     $rootScope.checkConnection = function() 
     {
 	/* if not connected return true*/
	if(navigator.network.connection.type == Connection.NONE)
	{	
	//	alert('Please check your Internet Connection');	
		navigator.notification.alert('Please check your Internet Connection', function(){}, "Warning", "");
    		$location.path('/home');
	}
	else
	{
		
	}
						
     };  
    /* end of connection check */

     /* downloading data by open file native plugin*/
     $rootScope.downloaddata = function(url) 
     {
 		window.openFileNative.open(url); 	
     };
     
     /* open photogallery*/ 
     $rootScope.opengallery = function(imgid) 
 	{
 		var fpath = 'http://www.shreepadgroup.com/photogallery.php?imgid=' + imgid;
 		
 		var ref = window.open(fpath, '_blank', 'location=no');	
 		ref.addEventListener('loadstart', function(event) 
 		{ navigator.notification.activityStart("Loading", "Please wait for a while."); }
 		);
         	ref.addEventListener('loadstop', function(event) { navigator.notification.activityStop(); 
		
         	});
     };
   /* end of photogallery */
	
     /* open floorplangallery*/ 
     $rootScope.floorplangallery = function(projectid,planid) 
 	{
		
 		var fpath = 'http://www.shreepadgroup.com/floorplangallery.php?projectid=' + projectid + '&floorid=' + planid;
 		var ref = window.open(fpath, '_blank', 'location=no');	
 		ref.addEventListener('loadstart', function(event) 
 		{ navigator.notification.activityStart("Loading", "Please wait for a while."); }
 		);
         	ref.addEventListener('loadstop', function(event) { navigator.notification.activityStop(); 
		         	
         	});
     };
    /* end of floorplan gallery*/
    
    /* open projectgallery*/  
     $rootScope.projectgallery = function(projectid) 
     {
 		var fpath = 'http://www.shreepadgroup.com/projectgallery.php?projectid=' + projectid;
 		var ref = window.open(fpath, '_blank', 'location=no');	
 		ref.addEventListener('loadstart', function(event) 
 		{ navigator.notification.activityStart("Loading", "Please wait for a while."); }
 		);
         	ref.addEventListener('loadstop', function(event) { 
         	navigator.notification.activityStop();     	
         	 }); 
     };
    /* end of projectgallery*/
     $rootScope.backfun = function() {
    		$location.path('/home');
      }
      $rootScope.subtitle = function(title) 
   	{
 		document.getElementById('sub-title').innerHTML =title;
   	};
});

/* serailiaze data for sending to server */
function serialize(obj, prefix) {
    var str = [];
    for (var p in obj) {
      if (obj.hasOwnProperty(p)) {
        var k = prefix ? prefix + "[" + p + "]" : p,
              v = obj[p];
          str.push(typeof v == "object" ? serialize(v, k) : encodeURIComponent(k) + "=" + encodeURIComponent(v));
      }
    }
    return str.join("&");
}



shreepadApp.controller('mainController', function($scope,$http,$rootScope,$location,$window) {

	$scope.latest = null;
	$rootScope.title('Shreepad Group');
	$rootScope.header(true);
	$rootScope.footer(true);
	//$scope.imgarray = ["img/slider1.jpg","img/slider2.jpg","img/slider3.jpg","img/slider4.jpg"];
 	//$scope.myInterval = 3000;
	
 	document.addEventListener("backbutton", onBackKeyDown, false);

});

shreepadApp.controller('aboutController', function($scope,$http,$window,$rootScope,$location) {	
		//$rootScope.checkConnection();
    		$rootScope.title('About Us');
		$rootScope.header(true);
		$rootScope.footer(true);
    		$scope.about = "";
    		$scope.loader = true;
    		$scope.$watch('loader', function (newvalue, oldvalue, $scope) {	
  		     $scope.loader = newvalue;
            });
                       
  		$http.get(serviceurl+'about.php')
         	.success(function (data) {
            	 $scope.result = data;
            	 $scope.items = [
                    {
                        name: "item1",
                        desc: "Company Profile",
                        information : $scope.result.about
                    },
                    {
                        name: "item2",
                        desc: "Our Mission",
                        information : $scope.result.missionvision 
                    },
                    {
                        name: "item3",
                        desc: "Our Vision",
                        information : $scope.result.companyvalues
                    },
                    {
                        name: "item4",
                        desc: "Our Philosophy",
                        information : $scope.result.philosophy
                    },
                     
                ];
			//$scope.default = $scope.items[1];
			//$scope.$parent.isopen = ($scope.$parent.default === $scope.item);
			$scope.loader = false;
         		$scope.$watch('isopen', function (newvalue, oldvalue, $scope) {
                   		 $scope.$parent.isopen = newvalue;
         		});
       		 })
         	.error(function (data, status, headers, config) {
         	});
         	
    	
      document.removeEventListener("backbutton", onBackKeyDown, false);
});

shreepadApp.controller('success-storyController', function($scope,$rootScope,$http,$window,$location) {

			//$rootScope.checkConnection();
			$rootScope.title('Success Stories');
			$rootScope.header(true);
			$rootScope.footer(true);
			$scope.storyList = null;
			$scope.loader = true;
			$scope.$watch('loader', function (newvalue, oldvalue, $scope) {	
  		     		$scope.loader = newvalue;
  			});
  		 
			$http.get(serviceurl+'success-story.php')
         			.success(function (data) {
            	 		$scope.storyList = data;
            	 		$scope.loader = false;
        		 })
        		 .error(function (data, status, headers, config) {   
        		 });
          	
      	document.removeEventListener("backbutton", onBackKeyDown, false);
});

shreepadApp.controller('newsController', function($scope,$rootScope,$http,$window,$location) {
		//$rootScope.checkConnection();
		$scope.name="dishant";
		$rootScope.title('Our Timeline');
		$rootScope.header(true);
		$rootScope.footer(true);
		$scope.newsList = null;
		$scope.loader = true;

		$scope.$watch('loader', function (newvalue, oldvalue, $scope) {	
  		});

		$http.get(serviceurl+'news.php')
         		.success(function (data) {
             		$scope.newsList = data;
             		$scope.loader = false;
     		 })
      		.error(function (data, status, headers, config) {
       		});
     document.removeEventListener("backbutton", onBackKeyDown, false);
});


shreepadApp.controller('allprojectController', function($scope,$http,$window,$rootScope,$location) {   
        $rootScope.title('All Projects');
        $rootScope.header(true);
        $rootScope.footer(true);
        //$rootScope.checkConnection();
            $scope.loader = true;
            $scope.$watch('loader', function (newvalue, oldvalue, $scope) {   
               $scope.loader = newvalue;
          });
              
            $scope.imgappend = '';
            if(window.innerWidth <= 480){
            $scope.imgappend = "_mobile.jpg";
            }else {
            $scope.imgappend = "";
            }
           
            $scope.projectList = null;
            $http.get(serviceurl+'projectlist.php')
             .success(function (data) {
                 $scope.projectList = data;
                 $scope.loader = true;
             }).error(function (data, status, headers, config) {});
            
                
    
          document.removeEventListener("backbutton", onBackKeyDown, false);       
});


shreepadApp.controller('contactController', function($scope,$http,$route,$window,$rootScope,$location) {
	$rootScope.title('Inquiry');
	$rootScope.header(true);
	$rootScope.footer(true);
	$scope.loader = true;
	//$rootScope.checkConnection();
	setTimeout(function() {
    	$scope.$apply(function() {
     		$scope.loader = false;
    	});
  	}, 2000);	
  	
  	$scope.formInfo = {};
    	$scope.nameerror = false;
    	$scope.contacterror = false;
    	$scope.subjecterror = false;
    	$scope.messageerror = false;
    	$scope.emailerror = false;
    	$scope.errorfun= function (id) 
    	{
    		if(id == 1)	  { $scope.nameerror = true; }
    		else if(id == 2) { $scope.emailerror = true;  }
    		else if(id == 3) { $scope.contacterror = true; }
    		else if(id == 4) { $scope.subjecterror = true; }    
    		else if(id == 5) { $scope.messageerror = true; }    			
		else { }	
	};    
	$scope.formInfo.submitTheForm = function(item, event) {
       
     	var dataObject = {
          name : $scope.formInfo.name,
          email : $scope.formInfo.email,
          contact : $scope.formInfo.contact,
          subject : $scope.formInfo.subject,
          message : $scope.formInfo.message
     };
     
     //serialize data before sending
    var serdata = serialize(dataObject);
	$http({
    	  url: serviceurl+"sendcontact.php",
        method: "POST",
        headers: {'Content-Type': 'application/x-www-form-urlencoded'},
        data: serdata
    	}).success(function(data, status, headers, config) {
    		
    		if(data.msg == '1')
    		{
    			//$window.alert('Your inquiry has been send successfully...We will be in touch with you Soon...');
    			navigator.notification.alert('Your inquiry has been send successfully...We will be in touch with you Soon...', function(){}, "Success", "");
    		}
    		else
    		{
    			
    		}

			$route.reload();   	
    	}).error(function(data, status, headers, config) {	
    		//$window.alert('Submitting form failed!');	
    		navigator.notification.alert('Submitting form failed!', function(){}, "Failure", "");	
	});	   
     }		
	
	document.removeEventListener("backbutton", onBackKeyDown, false);
});

shreepadApp.controller('allprojectController', function($scope,$http,$window,$rootScope,$location) {	
		$rootScope.title('All Projects');
		$rootScope.header(true);
		$rootScope.footer(true);
		//$rootScope.checkConnection();
    		$scope.loader = true;
    		$scope.selectedstatus = '';
		$scope.selectedcat = 'Residential';
		
			
    		$scope.imgappend = '';
    		if(window.innerWidth <= 480){
    		$scope.imgappend = "_mobile.jpg";
    		}else {
    		$scope.imgappend = "";
    		}
    		
  			 setTimeout(function() {
    			$scope.$apply(function() {
     			$scope.loader = false;
    			});
  			}, 2000);	
  	  	
    		$scope.projectList = null;
    		$http.get(serviceurl+'projectlist.php')
     		.success(function (data) {
     			$scope.projectList = data;
     			console.log($scope.projectList);
     			
     		}).error(function (data, status, headers, config) {}); 
     		
     		$scope.changetype = function(type) 
     		{	
				if(type == 'All')
				{
					$scope.selectedstatus = '';
				}
				else
				{
					$scope.selectedstatus =  type;
				}
				$rootScope.title(type + ' Projects');
				$scope.status.isopen = !$scope.status.isopen;		
      		};

		$scope.changecat = function(cat) 
     		{	
				
			$scope.selectedcat = cat;		
      		};	
     
      	document.removeEventListener("backbutton", onBackKeyDown, false);		
});

shreepadApp.controller('reachusController', function($scope,$rootScope,$location) {
	$rootScope.header(true);
	$rootScope.footer(true);
	$scope.info 		= true;
	$scope.socialmedia 	= false;
	$scope.ratecontent 	= false;
	$rootScope.title('Reach Us');	
	//$rootScope.checkConnection();
	
	$scope.changeview = function(selectedtab) {       
     			if(selectedtab == 'info')
     			{
     				$scope.info 		= true;
				$scope.socialmedia 	= false;
				$scope.ratecontent 	= false;
			
     			}
			else if(selectedtab == 'contact')  
			{
				$scope.info 		= false;
				$scope.socialmedia 	= false;
				$scope.ratecontent 	= false;
			} 	
			else if(selectedtab == 'socialmedia')  
			{
				$scope.info 		= false;
				$scope.socialmedia 	= true;
				$scope.ratecontent 	= false;
			}  
			else if(selectedtab == 'ratecontent')  
			{
				$scope.info 		= false;
				$scope.socialmedia 	= false;
				$scope.ratecontent 	= true;
			}
	}    

  	document.removeEventListener("backbutton", onBackKeyDown, false);	 	  
});


shreepadApp.controller('projectDetailController', function($scope,$http,$routeParams,$rootScope,$location) {
	//$rootScope.checkConnection();
	 $rootScope.header(true);
	 $rootScope.footer(true);
	 $scope.downloading = false ; 
	 $scope.downloadmsg = '';
    	 $scope.projectDetail = null;
    	 $scope.projectId = $routeParams.projectId;
    	 $scope.display = true;
    	 $scope.code = null;
    	 $scope.loader = true;
	$scope.$watch('loader', function (newvalue, oldvalue, $scope) {	
  		     $scope.loader = newvalue;
        });	

	$scope.doverview 	= true;
	$scope.dfloorplan 	= false;
	$scope.dlocation 	= false;
	$scope.dbrochure 	= false;
	$scope.dvideo 		= false;
	$scope.dshare		= false;
	$scope.imgappend = '';
   	if(window.innerWidth <= 480){
    		$scope.imgappend = "_mobile.jpg";
   	 }else {
    		$scope.imgappend = "";
    	}
     	
	var dataObject = 
	{
          	projectid : $scope.projectId 
       	};
      // serialize data before sending       
      	var serdata = serialize(dataObject);
		$http({
    	  	url: "http://www.shreepadgroup.com/data/projectdetail.php",
        	method: "POST",
        	headers: {'Content-Type': 'application/x-www-form-urlencoded'},
        	data: serdata
    	   	}).success(function(data, status, headers, config) {
      		$scope.projectDetail = data;
		$rootScope.title($scope.projectDetail.title);
		 $scope.items = [
                    {
                        name: "item1",
                        desc: "Overview",
			information : $scope.projectDetail.overview
                      
                    },
                    {
                        name: "item2",
                        desc: "Amenities",
			information : $scope.projectDetail.amenities
                      
                    },
                    {
                        name: "item3",
                        desc: "Specification",
			information : $scope.projectDetail.specification
                      
                    },
                     
                ];
			//$scope.default = $scope.items[1];
			//$scope.$parent.isopen = ($scope.$parent.default === $scope.item);

			$scope.loader = false;
         		$scope.$watch('isopen', function (newvalue, oldvalue, $scope) {
                   		 $scope.$parent.isopen = newvalue;
         		});
      	
      	$scope.floorarray = $scope.projectDetail.floorplan.split(',');
      	$scope.floortitlearray = $scope.projectDetail.floorplantitle.split(',');
      	$scope.code = $scope.projectDetail.videourl;
      	
      	if($scope.floorarray == '')
      	{
      		$scope.display = false;
      	}
      
    		}).error(function(data, status, headers, config) {
     	
		});
		
		$scope.changeview = function(selectedtab) {       
     			if(selectedtab == 'overview')
     			{
				
     					$scope.doverview 	= true;
					$scope.dfloorplan 	= false;
					$scope.dlocation 	= false;
					$scope.dbrochure 	= false;
					$scope.dvideo 		= false;
					$scope.dshare		= false;
			
     			}
				else if(selectedtab == 'floorplan')  
				{
					$scope.doverview 	= false;
					$scope.dfloorplan 	= true;
					$scope.dlocation 	= false;
					$scope.dbrochure 	= false;
					$scope.dvideo 		= false;
					$scope.dshare		= false;
				} 
				else if(selectedtab == 'brochure')  
				{
					navigator.notification.confirm(
            					'Do You want to download the brochure?',  
            					function(button)
						{  if(button == 1){ openlinks($scope.projectDetail.brochure); }
       						},        
            					'Brochure Download',            
            					'Yes,No'        
       					 );	
        				
				}  
				else if(selectedtab == 'location')  
				{
					$scope.doverview 	= false;
					$scope.dfloorplan 	= false;
					$scope.dlocation 	= true;
					$scope.dbrochure 	= false;
					$scope.dvideo 		= false;
					$scope.dshare		= false;
				}  
				else if(selectedtab == 'video')  
				{
					//YoutubeVideoPlayer.openVideo($scope.projectDetail.videourl);
				}  
				else if(selectedtab == 'share')  
				{
					
					var strInputCode = $scope.projectDetail.productshortdesc;
					strInputCode = strInputCode.replace(/&(lt|gt);/g, function (strMatch, p1){
						return (p1 == "lt")? "<" : ">";
					});
					var overviewtxt = strInputCode.replace(/<\/?[^>]+(>|$)/g, "");
					
					
					var imgsrc = $scope.projectDetail.gallery + $scope.imgappend ; 
					
					window.plugins.socialsharing.share(overviewtxt,$scope.projectDetail.title, imgsrc, $scope.projectDetail.weburl);
					
				}    
       }
  	 $scope.backfun = function() {
      			$location.path('/ProjectSelect');
      } 
     document.removeEventListener("backbutton", onBackKeyDown, false);	
});


